package aula3;

import aula1.Horario;
import aula1.IHorario;
import aula2.Data;

public class Calendario {

	private IHorario horario;
	
	private Data data;
	
	public Calendario() {
		this.horario = new Horario();
		this.data = new Data();
	}
	
	public Calendario(int dia, int mes, int ano) {
		this.horario = new Horario();
		this.data = new Data(dia, mes, ano);
	}
	
	
	public void addSegundo() {
		if(this.horario.ehUltimoSegundo()) {
			this.data.addDia();
		}

		this.horario.addSegundo();
	}
	
	@Override
	public String toString() {
		return data + " " + horario;
	}
}
