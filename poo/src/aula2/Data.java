package aula2;

public class Data {

	private int ano;
	
	private int mes;
	
	private int dia;
	
	public Data(){
		ano = 1;
		mes = 1;
		dia = 1;
	}
	
	public Data(int d, int m, int a) {
		this();
		setAno(a);
		setMes(m);
		setDia(d);
	}
	
	public void addAno() {
		int a = ano + 1;
		
		if(a == 10000) {
			a = 1;
		}
		
		ano = a;
	}
	
	public void setAno(int a) {
		if(a >= 1 && a <= 9999) {
			ano = a;
		}
	}

	public void addMes() {
		int m = mes + 1;
		
		if(m == 13) {
			m = 1;
			addAno();
		}
		
		mes = m;
	}
	
	public void setMes(int m) {
		if(m >= 1 && m <= 12) {
			mes = m;
		}
	}

	public void addDia() {
		int d = dia + 1;
		
		int limite = calcularUltimoDia(mes, ano) + 1;
		
		if(d == limite) {
			d = 1;
			addMes();
		}
		
		dia = d;
	}
	
	public void setDia(int d) {
		
		int ultimo = calcularUltimoDia(mes, ano);
		
		if(d >= 1 && d <= ultimo) {
			dia = d;
		}
	}

	private int calcularUltimoDia(int mes, int ano) {
		int ultimo[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		
		if(mes == 2 && ehBissexto(ano)) {
			return 29;
		}
		
		return ultimo[mes];
	}

	private boolean ehBissexto(int ano) {
		return (ano % 400 == 0) || (ano % 100 != 0 && ano % 4 == 0);
	}

	@Override
	public String toString() {
		return dia + "/" + mes + "/" + ano;
	}
}
