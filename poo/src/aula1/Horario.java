package aula1;

public class Horario implements IHorario {

	private int hora;
	
	private int minuto;
	
	private int segundo;
	
	public Horario() {
	}
	
	@Override
	public void setHora(int h){
		if(h >= 0 && h <= 23) {
			hora = h;
		}
	}
	
	@Override
	public void addHora() {
		int h = hora + 1;
		
		if(h == 24) {
			h = 0;
		}
		
		hora = h;
	}
	
	@Override
	public void addHora(int h) {
		for(int i = 0; i < h; i++) {
			addHora();
		}
	}
	
	@Override
	public void setMinuto(int m){
		if(m >= 0 && m <= 59) {
			minuto = m;
		}
	}
	
	@Override
	public void addMinuto() {
		int m = minuto + 1;
		
		if(m == 60) {
			m = 0;
			addHora();
		}
		
		minuto = m;
	}
	
	@Override
	public void addMinuto(int m) {
		for(int i = 0; i < m; i++) {
			addMinuto();;
		}
	}
	
	@Override
	public void setSegundo(int s){
		if(s >= 0 && s <= 59) {
			segundo = s;
		}
	}
	
	@Override
	public void addSegundo() {
		int s = segundo + 1;
		
		if(s == 60) {
			s = 0;
			addMinuto();
		}
		
		segundo = s;
	}
	
	@Override
	public void addSegundo(int s) {
		for(int i = 0; i < s; i++) {
			addSegundo();
		}
	}
	
	@Override
	public String toString() {
		return hora + ":" + minuto + ":" + segundo;
	}

	@Override
	public boolean ehUltimoSegundo() {
		return hora == 23 && minuto == 59 && segundo == 59;
	}
}
