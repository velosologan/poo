package aula1;

public interface IHorario {

	void setHora(int h);

	void addHora();

	void addHora(int h);

	void setMinuto(int m);

	void addMinuto();

	void addMinuto(int m);

	void setSegundo(int s);

	void addSegundo();
	
	boolean ehUltimoSegundo();

	void addSegundo(int s);
}