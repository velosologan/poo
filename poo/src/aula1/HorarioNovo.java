package aula1;

public class HorarioNovo implements IHorario {

	private int segundos;
	
	@Override
	public void setHora(int h) {
		if(h >= 0 && h <= 23) {
			segundos += h * 60 * 60;
		}
		
	}

	@Override
	public void addHora() {
		this.addMinuto(60);
	}

	@Override
	public void addHora(int h) {
		for(int i = 0; i < h; i++) {
			this.addHora();
		}
	}

	@Override
	public void setMinuto(int m) {
		if(m >= 0 && m <= 59) {
			segundos += m * 60;
		}
		
	}

	@Override
	public void addMinuto() {
		this.addSegundo(60);		
	}

	@Override
	public void addMinuto(int m) {
		for(int i = 0; i < m; i++) {
			this.addMinuto();
		}
	}

	@Override
	public void setSegundo(int s) {
		if(s >= 0 && s <= 59) {
			this.segundos += s;
		}
	}

	@Override
	public void addSegundo() {
		int s = segundos + 1;
		
		if(s == 86400) {
			s = 0;
		}
		segundos = s;		
	}

	@Override
	public void addSegundo(int s) {
		for(int i = 0; i < s; i++) {
			this.addSegundo();
		}
	}
	
	public int getHora() {
		return segundos / 3600;
	}
	
	@Override
	public String toString() {
		int h = getHora();
		int m = getMinuto();
		int s = getSegundos();
		
		return String.valueOf(h + ":" + m + ":" + s);
	}

	private int getSegundos() {
		return segundos % 60;
	}

	private int getMinuto() {
		return segundos / 60 % 60;
	}

	@Override
	public boolean ehUltimoSegundo() {
		return segundos == 86399;
	}

}
