package aula1;

public class Principal {
	public static void main(String[] args) {
		
		IHorario h1 = new HorarioNovo();
		IHorario h2 = new Horario();
		System.out.print(h1 + "<->" + h2 + "\n");
		
		for(int i = 0; i < 86400; i++) {
			h1.addSegundo();
			h2.addSegundo();
			System.out.print(h1 + "<->" + h2 + "\n");
		}
	}
}
