package excecao;

public class Main {
	public static void main(String[] args) throws Exception {

		int a = 10;
		
		int b = 2;
		
		int c = dividir(a, b);
		
		System.out.println(c);	
	}

	private static int dividir(int a, int b) throws Exception {
		try {
			return a / b;
			
		} catch (RuntimeException e) {
			
			throw new DivisaoPorZeroException(e);
			
		} finally {
			System.out.println("fim");
		}
	}
}
