package excecao;

public class DivisaoPorZeroException extends Exception {

	public DivisaoPorZeroException(Exception e) {
		super("Divisão por zero", e);
	}
}
